CREATE TABLE registration_log (
	id serial8 PRIMARY KEY,
	company_id BIGINT,
	reg_id CHARACTER VARYING,
	code CHARACTER VARYING,
	message CHARACTER VARYING,
	content CHARACTER VARYING,
	efin CHARACTER VARYING,
	email_efin CHARACTER VARYING,
	username CHARACTER VARYING,
	password CHARACTER VARYING,
	phone CHARACTER VARYING,
	asp_id CHARACTER VARYING,
	registrar_name CHARACTER VARYING,
	job_position CHARACTER VARYING,
	created_at TIMESTAMP WITHOUT TIME ZONE,
	updated_at TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE current_user_log (
	id serial8 PRIMARY KEY,
	user_id BIGINT,
	company_id BIGINT,
	activity CHARACTER VARYING,
	activity_object CHARACTER VARYING,
	created_at TIMESTAMP WITHOUT TIME ZONE,
	updated_at TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE efiling_code (
	code CHARACTER VARYING PRIMARY KEY,
	name CHARACTER VARYING
);

INSERT INTO efiling_code (code, name) VALUES
('F113201', 'PPh Pasal 21'),
('F113203', 'PPh Pasal 23'),
('F113204', 'PPh Pasal 4 ayat 2'),
('F113205', 'PPh Pasal 15'),
('F113214', 'PPh Badan'),
('F113215', 'PPh Badan Dolar'),
('F113216', 'PPh Orang Pribadi (OP)'),
('F123204', 'PPN')
;

CREATE TABLE efiling (
	id serial8 PRIMARY KEY,
	company_id BIGINT,
	tax_period_start CHARACTER VARYING,
	tax_period_end CHARACTER VARYING,
	tax_year CHARACTER VARYING,
	report_date TIMESTAMP WITHOUT TIME ZONE,
	status INTEGER,
	explanation CHARACTER VARYING,
	csv_file_name CHARACTER VARYING,
	pdf_file_name CHARACTER VARYING,
	is_csv_file_name_true BOOLEAN,
	is_pdf_file_name_true BOOLEAN,
	csv_file_data TEXT,
	pdf_file_data TEXT,
	upload_dir CHARACTER VARYING,
	ntpa CHARACTER VARYING,
	ntte CHARACTER VARYING,
	ntte_date TIMESTAMP WITHOUT TIME ZONE,
	nominal NUMERIC,
	queue_ticket CHARACTER VARYING,
	last_eleven_digits_efiling_code CHARACTER VARYING,
	efiling_code CHARACTER VARYING,
	tax_type CHARACTER VARYING,
	revision CHARACTER VARYING,
	created_at TIMESTAMP WITHOUT TIME ZONE,
	updated_at TIMESTAMP WITHOUT TIME ZONE,
	CONSTRAINT efiling_id_efiling_code_fkey FOREIGN KEY (efiling_code)
      REFERENCES efiling_code (code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE other_attachment_file (
	id serial8 PRIMARY KEY,
	efiling_id BIGINT,
	status INTEGER,
	pdf_file_name CHARACTER VARYING,
	pdf_file_data TEXT,
	created_at TIMESTAMP WITHOUT TIME ZONE,
	updated_at TIMESTAMP WITHOUT TIME ZONE,
	CONSTRAINT other_attachment_file_id_efiling_id_fkey FOREIGN KEY (efiling_id)
      REFERENCES efiling (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);