package com.efiling.pajak.io.service;

import java.util.List;

import com.efiling.pajak.io.model.OtherAttachmentFileModel;

public interface OtherAttachmentFileService {
	
	List<OtherAttachmentFileModel> findAllByEfilingId(long efilingId);
	List<String> findAllNameByEfilingId(long efilingId);
	void createOtherAttachmentFile(OtherAttachmentFileModel other);
}
