package com.efiling.pajak.io.service;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.efiling.pajak.io.model.EfilingModel;

public interface EfilingService {
	
	void storeFile(long companyId, long efilingId, MultipartFile file);
//	void storeFiles(MultipartFile[] files, long companyId, boolean isSptBadan);
	Resource loadFileAsResource(String fileName, long companyId, long efilingId);
	EfilingModel createEfiling(MultipartFile[] files, long companyId, boolean isSptBadan, LinkedHashMap<String, String> fileMap);
	void storeFiles(MultipartFile[] files, long companyId, boolean isSptBadan, LinkedHashMap<String, String> fileMap);
	void deleteEfiling(EfilingModel efiling);
	Optional<EfilingModel> findByIdAndCompanyId(long efilingId, long companyId);
	void deleteDirectory(long companyId, long efilingId) throws IOException;
	EfilingModel updateEfiling(long companyId, long efilingId, List<Long> otherFilesId, boolean isSptBadan);
	List<EfilingModel> getFilingListPage(long companyId, String taxYear, int taxPeriod, int pageNumber);
	CompletableFuture<Boolean> sendToProcessingFile(EfilingModel efiling);
	String getCookieFromGateway();
	void saveEfiling (EfilingModel efiling);
}