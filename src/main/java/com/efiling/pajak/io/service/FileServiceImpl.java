package com.efiling.pajak.io.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.efiling.pajak.io.exception.FileNotFoundException;
import com.efiling.pajak.io.exception.FileStorageException;
import com.efiling.pajak.io.model.FileModel;
import com.efiling.pajak.io.repository.FileRepository;

//import org.apache.commons.codec.binary.Base64;

@Component
public class FileServiceImpl implements FileService {
	
	private final Path fileStorageLocation;
	
	@Autowired
	public FileServiceImpl(FileModel file) {
		this.fileStorageLocation = Paths.get(file.getUploadDir()).toAbsolutePath().normalize();
//		this.fileStorageLocation = Paths.get(file.getUploadDir()).normalize();
		System.out.println(this.fileStorageLocation.toString());
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception exc) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", exc);
		}
	}
	
	@Autowired
	FileRepository fileDb;

	@Override
	public String storeFile(MultipartFile file) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String extensionRemoved = StringUtils.cleanPath(file.getOriginalFilename())
				.split("\\.")[0];
		System.out.println(extensionRemoved);
		
		try {
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			
			return fileName;
		} catch (IOException exc) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", exc);
		}
	}

	@Override
	public Resource loadFileAsResource(String fileName) {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException("File not found: " + fileName);
			}
		} catch (MalformedURLException exc) {
			throw new FileNotFoundException("File not found: " + fileName, exc);
		}
	}

	@Override
	public String base64Encoder(String fileName) {
		String encodedString = Base64.getEncoder().encodeToString(fileName.getBytes());
		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
		String decodedString = new String(decodedBytes);
		System.out.println(encodedString);
		System.out.println("HI");
		System.out.println(decodedString);
		// TODO Auto-generated method stub
		return encodedString;
	}

	@Override
	public String base64Decoder(String encodedFileName) {
		byte[] decodedBytes = Base64.getDecoder().decode(encodedFileName);
		String decodedString = new String(decodedBytes);
		return decodedString;
	}
}
