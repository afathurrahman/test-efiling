package com.efiling.pajak.io.service;

import com.efiling.pajak.io.model.EfilingModel;

public interface FileProcessService {
	
	String generateNtpa();
//	String generateChunkId();
//	void sendToFileProcess(EfilingModel efiling);
	String getJenisKirim(EfilingModel efiling);
	String getFilePath(String strFilePath);
	String base64Encoder(String fileName);
	String base64Decoder(String encodedFileName);
	String fileNameConverter();
}
