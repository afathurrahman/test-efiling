package com.efiling.pajak.io.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.efiling.pajak.io.exception.FileNotFoundException;
import com.efiling.pajak.io.exception.FileStorageException;
import com.efiling.pajak.io.model.EfilingCodeModel;
import com.efiling.pajak.io.model.EfilingModel;
import com.efiling.pajak.io.model.FileModel;
import com.efiling.pajak.io.model.OtherAttachmentFileModel;
import com.efiling.pajak.io.repository.EfilingCodeRepository;
import com.efiling.pajak.io.repository.EfilingRepository;
import com.efiling.pajak.io.repository.OtherAttachmentFileRepository;

@Component
public class EfilingServiceImpl implements EfilingService {

	private final Path fileStorageLocation;
	
	@Autowired
	public EfilingServiceImpl(FileModel file) {
		this.fileStorageLocation = Paths.get(file.getUploadDir()).toAbsolutePath().normalize();
//		this.fileStorageLocation = Paths.get(file.getUploadDir()).normalize();
		System.out.println(this.fileStorageLocation.toString());
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception exc) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", exc);
		}
	}
	
	@Autowired
	EfilingRepository efilingDb;
	
	@Autowired
	EfilingCodeRepository efilingCodeDb;
	
	@Autowired
	OtherAttachmentFileRepository otherAttachmentFileDb;
	
	@Autowired
	FileProcessService fileProcessService;
	
	@Autowired
	OtherAttachmentFileService otherAttachmentFileService;
	
	@Value("${efiling.gateway.cookie}")
    private String getCookieUri;
	
	@Value("${efiling.pf}")
    private String procFileUri;
	
	@Override
	public void storeFiles(MultipartFile[] files, 
			long companyId, 
			boolean isSptBadan,
			LinkedHashMap<String, String> fileMap) {
		
		EfilingModel efiling = createEfiling(files, companyId, isSptBadan, fileMap);
		
		for (MultipartFile file: files) {
			String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//			String[] fileNameAndExt = fileName.split("\\.");
//			String encodedFileName = fileProcessService.base64Encoder(fileNameAndExt[0]);
			
			try {
				if (fileName.contains("..")) {
					throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
				}
				
				Path specificLocation = Paths.get(
						this.fileStorageLocation.toString() +
						"/" +
						Long.toString(companyId) +
						"/" +
						Long.toString(efiling.getId())
						).toAbsolutePath().normalize();
				
				Files.createDirectories(specificLocation);
				
//				Path targetLocation = this.fileStorageLocation.resolve(fileName);
				Path targetLocation = specificLocation.resolve(fileName);
//				Path targetLocation = specificLocation.resolve(encodedFileName + "." + fileNameAndExt[1]);
				Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException exc) {
				throw new FileStorageException("Could not store file " + fileName + ". Please try again!", exc);
			}
		}
	}
	
	@Override
	public EfilingModel createEfiling(
			MultipartFile[] files, 
			long companyId, 
			boolean isSptBadan,
			LinkedHashMap<String, String> fileMap) {
		
		EfilingModel efiling = new EfilingModel();
		
		// Acuan dari file csv saja
		String[] fileNameAndExt = StringUtils.cleanPath(files[0].getOriginalFilename()).split("\\.");
		
//		String npwp = fileNameAndExt[0].substring(0, 15);
		String taxPeriodStart = fileNameAndExt[0].substring(15, 17);
		String taxPeriodEnd = fileNameAndExt[0].substring(17, 19);
		String taxYear = fileNameAndExt[0].substring(19, 23);
		String sptRevision = fileNameAndExt[0].substring(23, 25);
		String efilingCode = fileNameAndExt[0].substring(25, 32);
		EfilingCodeModel getEfilingCode = efilingCodeDb.findByCode(efilingCode);
		String lastElevenDigitsEfilingCode = fileNameAndExt[0].substring(25, fileNameAndExt[0].length() - 1);
		
		efiling.setCompanyId(1);
		efiling.setTaxPeriodStart(taxPeriodStart);
		efiling.setTaxPeriodEnd(taxPeriodEnd);
		efiling.setTaxYear(taxYear);
		efiling.setStatus(0); // 0=Draft, 1=Wait, 2=Success, 3=Failed
		efiling.setQueueTicket(ticketGenerator());
		efiling.setLastElevenDigitsEfilingCode(lastElevenDigitsEfilingCode);
		efiling.setEfilingCode(efilingCode);
		efiling.setTaxType("SPT " + getEfilingCode.getName() + " " + taxYear);
		efiling.setRevision(sptRevision);
		
		efiling.setCsvFileName(fileNameAndExt[0]);
		efiling.setCsvFileNameTrue(true);
		
		efilingDb.save(efiling);
		
		efiling.setCsvFileData(
				new JSONObject()
				.put("id", "eFilingUploaded/" + Long.toString(companyId) + "/" + Long.toString(efiling.getId()) + "/" + StringUtils.cleanPath(files[0].getOriginalFilename()))
//				.put("storage", "cache")
				.put("metadata", 
						new JSONObject()
						.put("filename", StringUtils.cleanPath(files[0].getOriginalFilename()))
						.put("size", files[0].getSize())
						.put("mime_type", "text/csv")
					)
				.toString()
				);
		
		if (files.length == 2) {
			// Both csv and pdf in efiling
			
			efiling.setPdfFileName(fileNameAndExt[0]);
			efiling.setPdfFileNameTrue(true);
			efiling.setPdfFileData(
					new JSONObject()
					.put("id", "eFilingUploaded/" + Long.toString(companyId) + "/" + Long.toString(efiling.getId()) + "/" + StringUtils.cleanPath(files[1].getOriginalFilename()))
//					.put("storage", "cache")
					.put("metadata", 
							new JSONObject()
							.put("filename", StringUtils.cleanPath(files[1].getOriginalFilename()))
							.put("size", files[1].getSize())
							.put("mime_type", "application/pdf")
						)
					.toString()
					);
			efilingDb.save(efiling);
//		} 
//			else if (files.length == 2 && isSptBadan == true) {
//			// Csv in efiling,  pdf in other attachment file
			
		} else if (files.length > 2) { // SPT PPh Badan
			// Csv in efiling, pdfs in other attachment  file
			
//			System.out.println(files[1].getContentType());
//			System.out.println(files[1].getName());
//			System.out.println(files[1].getOriginalFilename());
//			System.out.println(files[1].getSize());
//			System.out.println(files[1].getResource());
//			System.out.println(files[1].getClass());
			
			efiling.setPdfFileName(fileNameAndExt[0]);
			efiling.setPdfFileNameTrue(true);
			efiling.setPdfFileData(
					new JSONObject()
					.put("id", "eFilingUploaded/" + Long.toString(companyId) + "/" + Long.toString(efiling.getId()) + "/" + StringUtils.cleanPath(files[1].getOriginalFilename()))
//					.put("storage", "cache")
					.put("metadata", 
							new JSONObject()
							.put("filename", StringUtils.cleanPath(files[1].getOriginalFilename()))
							.put("size", files[1].getSize())
							.put("mime_type", "application/pdf")
						)
					.toString()
					);
			efilingDb.save(efiling);
			
			for (int i = 2; i < files.length; i++) {
				String[] otherFileNameAndExt = StringUtils.cleanPath(files[i].getOriginalFilename()).split("\\.");
				OtherAttachmentFileModel otherAttachment = new OtherAttachmentFileModel();
				
				otherAttachment.setEfilingId(efiling.getId());
				otherAttachment.setStatus(efiling.getStatus());
				otherAttachment.setPdfFileName(otherFileNameAndExt[0]);
				otherAttachment.setPdfFileData(
						new JSONObject()
						.put("id", "eFilingUploaded/" + StringUtils.cleanPath(files[i].getOriginalFilename()))
						.put("storage", "cache")
						.put("metadata", 
								new JSONObject()
								.put("filename", StringUtils.cleanPath(files[i].getOriginalFilename()))
								.put("size", files[i].getSize())
								.put("mime_type", "application/pdf")
							)
						.toString()
						);
				otherAttachmentFileDb.save(otherAttachment);
			}
		}
		
		return efiling;
	}
	
	@Override
	public void storeFile(long companyId, long efilingId, MultipartFile file) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		String[] fileNameAndExt = fileName.split("\\.");
//		String encodedFileName = fileProcessService.base64Encoder(fileNameAndExt[0]);
		
		try {
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			
			Path specificLocation = Paths.get(
					this.fileStorageLocation.toString() +
					"/" +
					Long.toString(companyId) +
					"/" +
					Long.toString(efilingId)
					).toAbsolutePath().normalize();
			
			Files.createDirectories(specificLocation);
			
//			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Path targetLocation = specificLocation.resolve(fileName);
//			Path targetLocation = specificLocation.resolve(encodedFileName + "." + fileNameAndExt[1]);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException exc) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", exc);
		}
	}

	@Override
	public Resource loadFileAsResource(String fileName, long companyId, long efilingId) {
		try {
			Path filePath = Paths.get(
					this.fileStorageLocation.toString() +
					"/" +
					Long.toString(companyId) +
					"/" +
					Long.toString(efilingId)
					).resolve(fileName).normalize();
			
//			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			
			Resource resource = new UrlResource(filePath.toUri());
			
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException("File not found: " + fileName);
			}
		} catch (MalformedURLException exc) {
			throw new FileNotFoundException("File not found: " + fileName, exc);
		}
	}
	
	public String ticketGenerator() {
		StringBuilder ticket = new StringBuilder("ftx-13-5-");
		String alphanumeric = "0123456789abcdefghijklmnopqrstuvxyz";
		int n = 14;
		
        for (int i = 0; i < n; i++) { 
            int index = (int)(alphanumeric.length() * Math.random()); 
            ticket.append(alphanumeric.charAt(index)); 
        } 
        
        return ticket.toString();
	}

	@Override
	public void deleteEfiling(EfilingModel efiling) {
		efilingDb.delete(efiling);
		
		try {
			deleteDirectory(efiling.getCompanyId(), efiling.getId());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void deleteDirectory(long companyId, long efilingId) throws IOException {
		Path target = Paths.get(
						this.fileStorageLocation.toString() +
						"/" +
						Long.toString(companyId) +
						"/" +
						Long.toString(efilingId)
						)
				.normalize();
		
		FileUtils.deleteDirectory(target.toFile());
	}
	
	@Override
	public Optional<EfilingModel> findByIdAndCompanyId(long efilingId, long companyId) {
		return efilingDb.findByIdAndCompanyId(efilingId, companyId);
	}

	@Override
	public EfilingModel updateEfiling(long companyId, long efilingId, List<Long> otherFilesId, boolean isSptBadan) {
		EfilingModel efilingUpdate = efilingDb.getOne(efilingId);
		efilingDb.save(efilingUpdate);
		
		if (isSptBadan == true) {
			for (Long otherId: otherFilesId) {
				otherAttachmentFileDb.save(otherAttachmentFileDb.getOne(otherId));
			}
		}
		
		return efilingUpdate;
	}

	@Override
	public List<EfilingModel> getFilingListPage(long companyId, String taxYear, int taxPeriod, int pageNumber) {
		int pageSize = 20;
		
//		taxPeriod value still 5 not 05, must be 05
		String strTaxPeriod = taxPeriodIntToString(taxPeriod);
	
		Pageable paging = PageRequest .of(pageNumber, pageSize);
		
		Page<EfilingModel> pagedResult = efilingDb.findByTaxYearAndTaxPeriodAndStatus(taxYear, strTaxPeriod, paging);
		
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<EfilingModel>();
		}
	}
	
	public String taxPeriodIntToString(int taxPeriod) {
		String sTaxPeriod = "";
		
		if (taxPeriod < 10) {
		    sTaxPeriod = "0" + String.valueOf(taxPeriod);
		} else {
		    sTaxPeriod = String.valueOf(taxPeriod);
		}
		
		return sTaxPeriod;
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public CompletableFuture<Boolean> sendToProcessingFile(EfilingModel efiling) {
//		String processingFileUri = "http://10.240.67.21:8080/pf01/process";
		
		System.out.println("Assalamualaikum");
		
		String processingFileUri = procFileUri;
		System.out.println("Proc. file uri: " + procFileUri);
				
		/*
		 * Request Headers
		 */
		String cookie = getCookieFromGateway();
		System.out.println("Cookie: " + cookie);
		
		String aspId = "016"; // 016
		String aspPassword = "1234567"; // 1234567
		
		System.out.println("Assalamualaikum 2");
		
		/*
		 * Dummy, must be get from company & user management
		 */
		String companySerialNumber = "334817197743516856";
		String companyNpwp = "844507533003000";
		String userPassword = "aaAA11!!";
		String companyBase64Wp = "MIISZAIBAzCCEh4GCSqGSIb3DQEHAaCCEg8EghILMIISBzCCAzQGCSqGSIb3DQEHAaCCAyUEggMh\\r\\nMIIDHTCCAxkGCyqGSIb3DQEMCgECoIICsjCCAq4wKAYKKoZIhvcNAQwBAzAaBBRoniI8rUbEsatY\\r\\nBMGyPTzlqhLoUQICBAAEggKAypHpglyRD3vLyawiR/PFjiqj3sP5mscbnsqJS5unR0+rZmFNhajh\\r\\nYhSq3BjZtkzqM3Lh2K5z369HmiEJ0/cbOK8kLIun+XW//Q+sbDa0hG7YZPFwtn0oQobcE/E6TGuj\\r\\nTAWpqgpTKhL1PHsqtjUob491BVVCI58cKg16R+8BlKyip77vYdAzjziSQFZ3S2Nl4Fm2jsBLXivJ\\r\\n29sYlAwPlcT1zm55EaDW+y65shDTbxAheMtqKC7QyXGXc1UV0QRgQF4ov5bxgZHo65lFw/wMxV4Q\\r\\nTfc3Ac7F3Wfe4BdNJDQVnr0EX/xRtHecWjzPR2QkKwWeAZNjknAXS8LUtJDz9ecZfsVRI8ZM/MNW\\r\\nc4HLhgcH07Og9fjYTRScupMn1N975W1bLLHWvfABpySekGAoc+LCtKOpjFeolM1cT3cyxCqxA+b7\\r\\nZshH0bVkaCmgGQq2EQ8W9Hj6gOIuYqyjyp10mc1qlmoMP8FYABxXsIkehWOPx/ni4H+6SvkxnvXi\\r\\n8Q7XGv3eZS0utcR1hyIRfaqf1jdJC5CTSVAVdQW81DVWNJcDi9u0jYLMzidiOG/3Bon9xOvpLmvg\\r\\nacHKH9JMOtd/SwPYcZ7n6KH6SyLrqCzSDPdzwPZuYNAb1WA5FVbaPrvRA0XnNVd5lE4tIgMluhJ6\\r\\nyDFQE8Fw6W/VbVnX0bA0qxeRNL4f4XtZQxzpokyjd2sodmqb3kFXhlsrmHIoHf7XLeXZIkmRBJnc\\r\\nO7aOwd2uw9V8Oib8QJQJA3Kz0OJUcvlNfZ6EEHfspIU47xxiiJ+DsTUcm644rSRr23MN03zTCNBN\\r\\ng+qcRQ/EXmZ8oYA2LapUNZ7QWSNSTxoKRYrckNd08DFUMCMGCSqGSIb3DQEJFTEWBBSSdilwRQvc\\r\\nLK70+Say94dE9kNeSDAtBgkqhkiG9w0BCRQxIB4eADAAMQAzADEAOQA2ADQAOAAwADAANwAzADAA\\r\\nMAAwMIIOywYJKoZIhvcNAQcGoIIOvDCCDrgCAQAwgg6xBgkqhkiG9w0BBwEwKAYKKoZIhvcNAQwB\\r\\nBjAaBBT8aFBpAqvV2VDpixAJEXLzT2zUugICBACAgg54BXj5rCP6WkLue/xvK0aBWrT3NQqNyMw8\\r\\nWi8L0AyNotA4SnJUfRXsnXgeulx0Otrp9/FD+CpWPbiYOMPaISA0+nAFaQ1I0hv/aMSc7N2OTa+E\\r\\nkic+bsqCa/yszfVaVuTR7uqEO4fzKQ0l16jkGRBn49QHfmD2aJrtmRTBK9y2a2AIp77syWfuVBh7\\r\\nRm/z6d6QSjoWOf/e/OT0MIWAc1e2o+CvCVHONehsbSQb0ieWYORZ6x3CjOJOhOXV6IJ2Y7UPnL6s\\r\\nk//52c6Wz2TUz3ScM+U3SzlREWqEgWMVxFdN+u+LLpsGHhyQhxa4ciCRxszBKotv4DhHHy9yB3I3\\r\\nUSk/hWtmVuVC+59Y0hYKatU08KlXLAiEhd6a0YUQydaxCpyJjaQRYab0XffXjiY2tvrHe6ZG7DdD\\r\\nogUkPnWpxRJpMJXeeQTy78v9b96dSoPJ2q9r+5W29caBy6rbBf+RXlaC/5zZn8EaQQEeSLWpNRJB\\r\\nVE2XRAy+nWqxWhcVvjlFpYVFx5DA4mZRaAsx582+clqp5AotjHxvyQxu8ztmrTQ4ToOi7O5o/Mr9\\r\\nlHaB6EWfZr3i9B8PUJJTtMfBioeebS4TI/Xdwy5eBLM/lZLoo4hL7GK+xGGianmZrpSwB5njkEOZ\\r\\n2hFx9cby2xp6YSz8enJBXSSxedCl4OS4BH+XkT489iuxx3NuI5RpmWgterGHUUC0sf9GCvxUboEi\\r\\nD/REIH7Gd3FjWnxqqgthw2b+ldQKzCgeaL7cUsSTNgZvEb+j0yfufL8Ra0c1U6y21sk6UksyPuNz\\r\\n0yS9o8P4wEe7w+FPIvPL1C1uGHRAg3yvPvT+pXLs50VAVKE3zrDPTzI/2l9VrzS4b837EtrjO+Bx\\r\\nzeSqOQsiu9ZjhZKoebzdagwpEvKX/p6vh4DmSb3e+4nfHIeue3i9LqSz+pfcFb0pXRnNsVXvF5SM\\r\\nqrNuIXjWUf/I6KTbI3GRtaL0s7uRPH4V1C7FEJX/YfyZQlGzwebvmHwEKSG+IF9S+1//8BSMelIt\\r\\na+mdynPg5HS+va0DRq8VlpaGVRmozvVJ6yY20OjJZXWXRHR6a0WUl7TnUZmkclbeRjV8S3GWOOtt\\r\\nwAqr9+cywy5dpmyxmyIo0PHCUbXwdN4lqs/QdBZME4Nv0Px8Wntpys9cSQvn69X/hAhvxdJJSJvY\\r\\nQYCMm7pqVIiwvUe7kI/WR/wy79E31GAHwtsdGK6Q71sr6Tx3GY640I251T5HZwuO7bqEeC0gqHUK\\r\\nkB8UntnBid6rbhhKGqjmXqXIm5MWQIIyDZXi7e5SfM3izRORc54i/gNXmqo3rktnoLmTBOXdkbes\\r\\nD9I9WbXa4J0yT/rv9peSG9DhzNwFaf/283LVAbnpMRXtK9Jmz0rVBwlRMoLiF7OkbItpsHC2g3gq\\r\\nJfYKJVnf9ND6mft0ykR4MtPISwx4pGHaTTOrumxUheL5fgBubF5VwYmcTwM+Di1cudqU+5QpD9eF\\r\\nX04Q3UdRQptcBSX001dGPHUl6az2bW8FCaBIoKM7GSCn1fZWKzvjQPRD+xoHnlzbD05ywxrTIsQv\\r\\n7tTh2uviWvTpGV6eTpKr19HKGrBWBDVamLVs26w0d3uIRMfQBA7/IDhgxDIKEtj65NiQ2Dyp7+Gj\\r\\nqp638pEjIDpJ7PZpN58pAo87lsuejOoqk5QNJiUUb6HD1HasBgw1GVbsT+8t7wqYjVVIUJZDI9xk\\r\\ntJc6ibEJFedgPEOaQAl3doHPB7IulTwCnwFqPeW+mY5afBy5hAjEHAC41oNdKpjaLCsAlxZKodSg\\r\\nls3MD2bAOs1FhRNjyIfFpFK7GAoFuoUBQYAOryoZy9hWaGN/xrVgUDd44GYZer4H0SevV4k72qgI\\r\\njlcACw+KxlD5z0Lg5OzYHKlgfhqq3EgItAQ7X4fdgFq0B1nAJ4iU0dbhTzCMMXgzB5l9YiRhuk/3\\r\\nE8SYWate1V4NGk36g2BclKyAjRXmx6uLsA3GKtrkpfcN0bs941z7+GUAN7+cLYpcIPhjaYize2w9\\r\\nCB79vTysMLOXn03W6tsfMQVaHX+VMnd4O0GgwAj0rQnV93V42sZ26nWlPEmMrMbwEzTaWipZT+85\\r\\nlY2TDa356rqXiTr4LeU5j8eBrm9xjd4utwyJmJr3rCN7j/bqSF+tTDu/4vyoFd864dKlKZaUi6y5\\r\\nxHmec2AZnMuUM7RUTnx48cwXe9mISbtI3AEBL9iIrq4+PUDQ3iGwtDmK2UaBF3yPEtFqFSG3fg8C\\r\\nxABtr1Jh0ycpS6ZVAtq50O6uDm+dSJ92Hufijlw/CJZJLCkmGOpo1nx4d9lOacWFzwPhacY4nJig\\r\\n6QI8zSKlPNNx7Tqz3IvkNPmFJycEPiI2PlIgYH4raA1suS6ayNseV1ZxphadaLGDOfYqqdVopWTC\\r\\nnDC6NRtDyX+VavGQTIU5vB+LPRlfQBY/fOQYZQmFmVjxbtFQSws7cJjJzRNGH3yz6GxPZYRLAFgc\\r\\n3LywcOC3KHpFCNU80ItGDGVxQvbr81wENGGD0YnjLD/cyCk6l17iMZF4Du4IGKDOglAkvEWA6+Oa\\r\\neoKw6sCuCrnxR4wSqWuEEpnNMNkD5OEbWTf6z72nmr6alADr6aJXGhH6DANZEcnAFeRIJgkO3HCc\\r\\nHi+Gw6IW9T783CwkGWkuJd9breeaJ3YW0yFkrbfCgoloVUtEIjRIwFEWx9cpu8mMhtkUfZcSH1BZ\\r\\nUHon5uqEZ8jylEfWwo2SKnQLGDdqLG75by/jtbKvAMTMJcRoYrdYu2MYcnehdllqOmHOsRcwYUom\\r\\nvpdfoc1ECTNX+Z9ODjLhzEaBBJ4r5XJXKIElgC7dbSIVIcJr6X7489uyTqgZdYIIo18sLOYndZcY\\r\\nbU31XzQXhUNcbM1zEZ+3t97/p/3MC5IGPQzO+nql8UIrdbdM9kKGJ82OX0gIUiRO4Yq0VdF7YYv5\\r\\nTsH3/4mTR/pq0X6eu/z05+ZUVbmLKMNgeQ6vT+37EeRqkvaECpbqLO4GFB83ipI7SzmlFiWMQ1Ep\\r\\nvBpJ1InH4v0DyID0urDqKLvFhsbMrBZG0JKSF8E8TkzewhabxP+VSfXVxhOtfxTJEKPvTP6DG4cE\\r\\nINiee/KjxQR13hOLpsy1cq4NP/sPxmSce5uHpddJFKosiJ48YXgukMQKcLv/C5bK1j2E1eiAKFVs\\r\\nqIaRL2l0muyDrbhaExVkjPKTGpdtQt7YV7EXTXhS5Ve+hYIaOMQj7I77i5H9s0BtgMVr6nqDpDPC\\r\\n1sFKcjkCjsSpMePYBdiF0Su30pQ2C4dYU/ScC8l81s3FNBZOevOZ+gUg7aOzq+2Fb1Uhcw1UBubN\\r\\nULOzk43I5g/68wsrxUORb2oNj6HwoVAItFDKUGYFiIu0LgIEnDyP8RZh+1g2frd1JBY9IMGRL6Lt\\r\\n9OJImu23kJVe80qY9zbAEDr1p2AFJXhdBtqBCHNwKypDo1QbBAGfGOOjG9mKsvU/4vuWMxnc6+9j\\r\\nGt3mNltW0u3zJh+4dBISl3EhKcY0gWNH5MBTqRNkJlu0GCD2wVEZKm1uXYHDhCvnyjLW7LoVU3LN\\r\\nscrbcB88niVwX4iofyD9u0rwCesgsT15gvxSXM7cUQRzGAXBHDqXYmOo8AFxWvdSf+8UMr+lwYXg\\r\\nNNJx7M5+jGLy7qAObJCbOZgGgL9mgEHpCGGgCthFLjX+eAUSXisaKvdSUmqMWHNKgQF3B8haOyZG\\r\\nX7nkB0240bXUoP14fYzSpLReytdYTgLD8YN1KYshZvg65odBL7P7Af0pRE8xycnXkHeHjeWYgSqx\\r\\nzHrLuOsCu8CBwLLdm+K8c6yJdvn3fWU1bpHBpo67R9oTa8WthGDQPH1w1eUKaIEu+xaSKHNsSnRJ\\r\\n2+ZvC/CHg84HT+MN9Nd3QDnfIIbCBM0dJHC9eJuDqMLOLxcOFSOX5cFTsIPGB7Aw/wBQ3ckrtyqO\\r\\nHA7jeS1o+Q0/Tt7rkEzEsasxHyA5e+GfP806Qy50gG1MLMm1gtxPcul/KTGEfbPIWGG2cUkK76h2\\r\\n20Obene48rtoKQBGWgdZTr90L3Drq+mJO7U2o5y1M5oVIljNUtr2EYLWL46Kr1Co6ZfPGfXYDKQN\\r\\n/hs/tuAaCfC5TeGzD78JOnGkgRHbSlJI00DKX5B4mffddSRqV9TVRJs0g8ALiF+hCFfEUwje30bW\\r\\nrm5krsuVtfBqtjnS41j+gWb32Vim4HaCEvEqwfWni1IQazth5gZuTdMDx8uLOmu880y6Nh/TvS6T\\r\\n16m6uJFJJDFDIBA4oryRT95uuhtkCv7aTE598OcnEe301oaIWCM0H8sSXa5Xcs2ELfwI3Ib5gxIJ\\r\\nKRwhtCw0Bo6q07cmmtTLCytJIptTjJvZCOF5/yBAQ9JoWNbg99qZvk+D7a64gd9jkJ7TK+ZaqTeP\\r\\nokr2yPpafMZHdPWqW1BEXk7xwA69fqBZhThy3XkzeQCszdWJIxXYM7FeJMiVPWD8U4KNJuD6KGeL\\r\\nWLqrtjuoXh23zDgm/+c4ulsxaj20t0qK9mO8R4hI9ddn4yZlsGdkW/WPDVBBA1c39zfqJN3tEirW\\r\\njn+OHJFKD4L2FXx4RCFeS7VWSdZqjXZC2GbW2t5sKynoMvUdXq0MO4Zyim3co29RO5bhWxtrKrM1\\r\\nmJY6RTEsf0mjvvPjVx+Wg8lp8iegbsYu4QuSBOBosHCWHCT92SGigLru7DW01R4wESlOeiDs9Dxm\\r\\nspDBujxOxJ0bneTbLV6NvSV6wpANtUM2FSnNcISMAr4QT0UkH9kGzacckzWw8Vs2sHkgRAY4nlrW\\r\\ny2fbI7KSKeJ9O11cLcmZTO58DP5i6aQfsX5LrUpCWZTRKLUKw/hW3nRnzpONTpjc0oRv8uVtiG12\\r\\nhliUs+C+0hrKqfe7lXiEq7kZ7Olft6nyhjLs40OQJZSOg4Sa97dz/ohRTHrfNoymW8fvaYs2sfNm\\r\\n9MfQjfVmy2XLFCafidr9Vx7D3jXwTFo6zgFKbWZ4fdcwPTAhMAkGBSsOAwIaBQAEFJlZ9ex0ydz2\\r\\neDQH75YEW6iln8EfBBTW9yhwcDtbjGPSd12dqsBr7bBe1AICBAA=\\r";
		
		System.out.println("Woi 0");
		
		/*
		 * Request Body
		 */
		HashMap<String, String> params = new HashMap<String, String>();
		System.out.println("Test 0");
		params.put("ntpa", fileProcessService.generateNtpa());
		System.out.println("Test 1");
		params.put("timestamp_wp", efiling.getReportDate().toString());
		System.out.println("Test 2");
		params.put("timestamp_asp", new Timestamp(System.currentTimeMillis()).toString());
		System.out.println("Test 3");
		params.put("jenis_kirim", fileProcessService.getJenisKirim(efiling));
		System.out.println("Test 4");
		params.put("path_csv", fileProcessService.getFilePath(efiling.getCsvFileData()));
		System.out.println("Test 5");
		params.put("path_pdf", fileProcessService.getFilePath(efiling.getPdfFileData()));
		System.out.println("Test 6");
		params.put("base64_wp", companyBase64Wp);
		System.out.println("Test 7");
		params.put("password", userPassword); // aaAA11!!
		System.out.println("Test 8");
		params.put("alias", companyNpwp);
		System.out.println("Test 9");
		params.put("serial_number", companySerialNumber);
		System.out.println("Test 10");
		params.put("nama_chunk", efiling.getCsvFileName());
		System.out.println("Test 11");
		params.put("jenis_pajak", (efiling.getEfilingCode().equalsIgnoreCase("F123204")) ? "F123" : efiling.getEfilingCode());
		
		System.out.println("Woi 1");
		
		if (efiling.getEfilingCode().equalsIgnoreCase("F113214")) {
			List<OtherAttachmentFileModel> otherAttachmentFiles = otherAttachmentFileService.findAllByEfilingId(efiling.getId());

			for (OtherAttachmentFileModel otherFile: otherAttachmentFiles) {
				if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("LK")) {
					params.put("path_lk", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("RK")) {
					params.put("path_rk", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("BUT")) {
					params.put("path_but", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("MGS")) {
					params.put("path_mgs", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("LU")) {
					params.put("path_lu", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else if (otherFile.getPdfFileName().substring(36, otherFile.getPdfFileName().length()).equalsIgnoreCase("DL")) {
					params.put("path_dl", fileProcessService.getFilePath(otherFile.getPdfFileData()));
				} else {
					;
				}
			}
		}
		
		System.out.println("Woi 2");
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", cookie);
		headers.add("aspId", aspId);
		headers.add("aspPassword", aspPassword);
		
		System.out.println("Woi 3");
		
		/*
		 * Request
		 */
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
		
		System.out.println("Woi 4");
		
//		processingFileUri = "http://localhost:8080/v2/test";
//		ResponseEntity<String> processResponse = restTemplate.postForEntity(processingFileUri, entity, String.class);
		System.out.println("Hi 0");
		ResponseEntity<String> processResponse = restTemplate.postForEntity(processingFileUri, entity, String.class);
		System.out.println("Hi 1");
		JSONObject responseBody = new JSONObject(processResponse.getBody());
		System.out.println("Hi 2");
		System.out.println("Message: " + responseBody.get("message"));
		System.out.println("Hi 3");
		System.out.println("Response Body: "  + responseBody);
		System.out.println("Hi 4");
		return CompletableFuture.completedFuture(false);
	}
	
	private String jUsername;
	private String jPassword;
	
	@Override
	public String getCookieFromGateway() {
		
		/*
		 * Sample Credentials
		 */
		jUsername = "016";
		jPassword = "1234567";
		
//		String securityCheckGatewayUri = "http://10.240.67.20:5000/fintaxgw/fgw2/Efiling2015/ws/j_spring_security_check";
		String securityCheckGatewayUri = getCookieUri;
		System.out.println("Get cookie uri: " + getCookieUri);
		
		RestTemplate restRequest = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("j_username", jUsername);
		params.add("j_password", jPassword);
		
		HttpEntity<LinkedMultiValueMap<String, String>> request = new HttpEntity<LinkedMultiValueMap<String, String>>(params, headers);
		
		ResponseEntity<String> response = restRequest.postForEntity(securityCheckGatewayUri, request, String.class);
		
		/*
		 * Get Cookie
		 * Full cookie response example: JSESSIONID=eZJks9W7oCFRhb1YUsq3mBcE; Path=/; Secure
		 */
		String cookieHeaderFull = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
		String cookie = cookieHeaderFull.split(";")[0];
		
		return cookie;
	}

	@Override
	public void saveEfiling(EfilingModel efiling) {
		efilingDb.save(efiling);
	}
}
