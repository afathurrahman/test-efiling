package com.efiling.pajak.io.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.efiling.pajak.io.model.OtherAttachmentFileModel;
import com.efiling.pajak.io.repository.OtherAttachmentFileRepository;

@Component
public class OtherAttachmentFileServiceImpl implements OtherAttachmentFileService {
	
	@Autowired
	OtherAttachmentFileRepository otherAttachmentFileDb;
	
	@Override
	public List<OtherAttachmentFileModel> findAllByEfilingId(long efilingId) {
		return otherAttachmentFileDb.findAllByEfilingId(efilingId);
	}

	@Override
	public List<String> findAllNameByEfilingId(long efilingId) {
		return otherAttachmentFileDb.findAllNameByEfilingId(efilingId);
	}

	@Override
	public void createOtherAttachmentFile(OtherAttachmentFileModel other) {
		otherAttachmentFileDb.save(other);
	}

}
