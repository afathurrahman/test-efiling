package com.efiling.pajak.io.service;

import java.util.Base64;
import java.util.Random;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.efiling.pajak.io.model.EfilingModel;
import com.efiling.pajak.io.repository.OtherAttachmentFileRepository;

@Component
public class FileProcessServiceImpl implements FileProcessService {
	
	@Autowired
	OtherAttachmentFileRepository otherAttachmentFileDb;
	
	@Override
	public String generateNtpa() {
		String res = "001";
		
		Random r = new Random();
		
		int low = 10000000;
		int high = 100000000;
		
		int range = r.nextInt(high-low) + low;
		
		return res + Integer.toString(range);
	}

	/*
	 * Default return 2, because 1 CSV & 1 PDF
	 */
	@Override
	public String getJenisKirim(EfilingModel efiling) {
		if (efiling.getEfilingCode().equalsIgnoreCase("F113214")) {
			int numberOfFiles = 2;
			numberOfFiles += otherAttachmentFileDb.countByEfilingId(efiling.getId());
			return Integer.toString(numberOfFiles);
		} else {
			return "2";
		}
	}
	
	/*
	 * strFilePath = csv/pdf_file_data
	 * {
	 * 		"metadata":
	 * 			{
	 * 				"filename":"8425596350030000112201800F1132140111.csv",
	 * 		 		"size":847,
	 * 				"mime_type":"text/csv"
	 * 			},
	 * 			"id":"eFilingUploaded/8425596350030000112201800F1132140111.csv",
	 * 			"storage":"cache"
	 * }
	 */
	@Override
	public String getFilePath(String strFilePath) {
		JSONObject jsonObj = new JSONObject(strFilePath);
		
		return jsonObj.get("id").toString();
	}

	@Override
	public String base64Encoder(String fileName) {
		String encodedString = Base64.getEncoder().encodeToString(fileName.getBytes());
		return encodedString;
	}

	@Override
	public String base64Decoder(String encodedFileName) {
		byte[] decodedBytes = Base64.getDecoder().decode(encodedFileName);
		String decodedString = new String(decodedBytes);
		return decodedString;
	}

	@Override
	public String fileNameConverter() {
		
		int fileNameLength = 32;
        String alphanumeric = "abcdefghijklmnopqrstuvxyz0123456789";
  
        StringBuilder convertedFileName = new StringBuilder(fileNameLength); 
  
        for (int i = 0; i < fileNameLength; i++) { 
            int index = (int)(alphanumeric.length() * Math.random()); 
            convertedFileName.append(alphanumeric.charAt(index)); 
        } 
        return convertedFileName.toString();
	}
}
