package com.efiling.pajak.io.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
	String storeFile(MultipartFile file);
	Resource loadFileAsResource(String fileName);
	String base64Encoder(String fileName);
	String base64Decoder(String encodedFileName);
}
