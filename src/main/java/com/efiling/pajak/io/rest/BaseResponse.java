package com.efiling.pajak.io.rest;

public class BaseResponse<T> {
	
	private T status;
	private String message;

	public T getStatus() {
		return status;
	}
	
	public void setStatus(T status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}


