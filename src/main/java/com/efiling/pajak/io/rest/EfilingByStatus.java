package com.efiling.pajak.io.rest;

import java.sql.Timestamp;
import java.util.LinkedHashMap;

public class EfilingByStatus {
	
	private String taxType;
	private String taxPeriodStart;
	private String taxPeriodEnd;
	private String taxYear;
	private Timestamp reportDate;
	private int status;
	private String queueTicket;
	private LinkedHashMap<String, Object> files;
	
	public String getTaxType() {
		return taxType;
	}
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	public String getTaxPeriodStart() {
		return taxPeriodStart;
	}
	public void setTaxPeriodStart(String taxPeriodStart) {
		this.taxPeriodStart = taxPeriodStart;
	}
	public String getTaxPeriodEnd() {
		return taxPeriodEnd;
	}
	public void setTaxPeriodEnd(String taxPeriodEnd) {
		this.taxPeriodEnd = taxPeriodEnd;
	}
	public String getTaxYear() {
		return taxYear;
	}
	public void setTaxYear(String taxYear) {
		this.taxYear = taxYear;
	}
	public Timestamp getReportDate() {
		return reportDate;
	}
	public void setReportDate(Timestamp reportDate) {
		this.reportDate = reportDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getQueueTicket() {
		return queueTicket;
	}
	public void setQueueTicket(String queueTicket) {
		this.queueTicket = queueTicket;
	}
	public LinkedHashMap<String, Object> getFiles() {
		return files;
	}
	public void setFiles(LinkedHashMap<String, Object> files) {
		this.files = files;
	}
}
