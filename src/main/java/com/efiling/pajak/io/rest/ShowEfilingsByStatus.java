package com.efiling.pajak.io.rest;

import java.util.List;

import com.efiling.pajak.io.model.EfilingModel;
import com.efiling.pajak.io.rest.Page;

public class ShowEfilingsByStatus {
	
	private List<EfilingModel> efilingList;
	private Page page;
	
	public List<EfilingModel> getEfilingList() {
		return efilingList;
	}
	public void setEfilingList(List<EfilingModel> efilingList) {
		this.efilingList = efilingList;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
}
