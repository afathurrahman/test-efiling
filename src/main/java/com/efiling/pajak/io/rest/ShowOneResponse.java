package com.efiling.pajak.io.rest;

import java.sql.Timestamp;
import java.util.LinkedHashMap;

//import com.fasterxml.jackson.annotation.JsonProperty;

public class ShowOneResponse {
	
	private String taxType;
	private String taxPeriod;
	private String taxYear;
	private String revision;
	private int status;
	private Timestamp reportDate;
	
//	@JsonProperty("files")
	private LinkedHashMap<String, Object> files;
	
	public String getTaxType() {
		return taxType;
	}
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	public String getTaxPeriod() {
		return taxPeriod;
	}
	public void setTaxPeriod(String taxPeriod) {
		this.taxPeriod = taxPeriod;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Timestamp getReportDate() {
		return reportDate;
	}
	public void setReportDate(Timestamp reportDate) {
		this.reportDate = reportDate;
	}
	public LinkedHashMap<String, Object> getFiles() {
		return files;
	}
	public void setFiles(LinkedHashMap<String, Object> files) {
		this.files = files;
	}
	public String getTaxYear() {
		return taxYear;
	}
	public void setTaxYear(String taxYear) {
		this.taxYear = taxYear;
	}
}
