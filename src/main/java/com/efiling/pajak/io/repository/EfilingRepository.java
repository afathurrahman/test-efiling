package com.efiling.pajak.io.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.efiling.pajak.io.model.EfilingModel;

public interface EfilingRepository extends JpaRepository<EfilingModel, Long> {
	
	Optional<EfilingModel> findByIdAndCompanyId(long efilingId, long companyId);
	
	@Query(value = "select * from efiling where tax_year=:tax_year and tax_period_start=:tax_period_start", nativeQuery = true)
	Page<EfilingModel> findByTaxYearAndTaxPeriodAndStatus(
			@Param("tax_year") String taxYear,
			@Param("tax_period_start") String taxPeriodStart,
			Pageable paging
			);
}
