package com.efiling.pajak.io.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.efiling.pajak.io.model.OtherAttachmentFileModel;

public interface OtherAttachmentFileRepository extends JpaRepository<OtherAttachmentFileModel, Long>{
	
	List<OtherAttachmentFileModel> findAllByEfilingId(long efilingId);
	
	@Query(value = "select pdf_file_name from other_attachment_file where efiling_id=:efiling_id", nativeQuery = true)
	List<String> findAllNameByEfilingId(@Param("efiling_id") long efilingId);
	
	@Query(value = "select count(*) from other_attachment_file where efiling_id=:efiling_id", nativeQuery = true)
	Integer countByEfilingId(@Param("efiling_id") long efilingId);
}
