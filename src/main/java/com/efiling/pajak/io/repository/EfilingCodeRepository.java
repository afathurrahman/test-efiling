package com.efiling.pajak.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.efiling.pajak.io.model.EfilingCodeModel;

public interface EfilingCodeRepository extends JpaRepository<EfilingCodeModel, String>{
	
	@Query(value = "select * from efiling_code where code=:code", nativeQuery = true)
	EfilingCodeModel findByCode(@Param("code") String code);
}
