package com.efiling.pajak.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.efiling.pajak.io.model.FileModel;

public interface FileRepository extends JpaRepository<FileModel, Long> {
	
}
