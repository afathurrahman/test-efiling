package com.efiling.pajak.io.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "efiling")
public class EfilingModel {
	
	private long id;
	private long companyId;
	private String taxPeriodStart;
	private String taxPeriodEnd;
	private String taxYear;
	private Timestamp reportDate;
	private int status;
	private String explanation;
	private String csvFileName;
	private String pdfFileName;
	private boolean isCsvFileNameTrue;
	private boolean isPdfFileNameTrue;
	private String csvFileData;
	private String pdfFileData;
	private String uploadDir;
	private String ntpa;
	private String ntte;
	private Timestamp ntteDate;
	private BigDecimal nominal;
	private String queueTicket;
	private String lastElevenDigitsEfilingCode;
	private String efilingCode;
	private String taxType;
	private String revision;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	
	@Id
	@Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "company_id")
	@JsonProperty("company_id")
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	@Column(name = "report_date")
	@JsonProperty("report_date")
	public Timestamp getReportDate() {
		return reportDate;
	}
	public void setReportDate(Timestamp reportDate) {
		this.reportDate = reportDate;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	
	@Column(name = "csv_file_name")
	@JsonProperty("csv_file_name")
	public String getCsvFileName() {
		return csvFileName;
	}
	public void setCsvFileName(String csvFileName) {
		this.csvFileName = csvFileName;
	}
	
	@Column(name = "pdf_file_name")
	@JsonProperty("pdf_file_name")
	public String getPdfFileName() {
		return pdfFileName;
	}
	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}
	
	@Column(name = "is_csv_file_name_true")
	@JsonProperty("is_csv_file_name_true")
	public boolean isCsvFileNameTrue() {
		return isCsvFileNameTrue;
	}
	public void setCsvFileNameTrue(boolean isCsvFileNameTrue) {
		this.isCsvFileNameTrue = isCsvFileNameTrue;
	}
	
	@Column(name = "is_pdf_file_name_true")
	@JsonProperty("is_pdf_file_name_true")
	public boolean isPdfFileNameTrue() {
		return isPdfFileNameTrue;
	}
	public void setPdfFileNameTrue(boolean isPdfFileNameTrue) {
		this.isPdfFileNameTrue = isPdfFileNameTrue;
	}
	
	@Column(name = "csv_file_data")
	@JsonProperty("csv_file_data")
	public String getCsvFileData() {
		return csvFileData;
	}
	public void setCsvFileData(String csvFileData) {
		this.csvFileData = csvFileData;
	}
	
	@Column(name = "pdf_file_data")
	@JsonProperty("pdf_file_data")
	public String getPdfFileData() {
		return pdfFileData;
	}
	public void setPdfFileData(String pdfFileData) {
		this.pdfFileData = pdfFileData;
	}
	
	@Column(name = "upload_dir")
	@JsonProperty("upload_dir")
	public String getUploadDir() {
		return uploadDir;
	}
	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}
	
	public String getNtpa() {
		return ntpa;
	}
	public void setNtpa(String ntpa) {
		this.ntpa = ntpa;
	}
	
	public String getNtte() {
		return ntte;
	}
	public void setNtte(String ntte) {
		this.ntte = ntte;
	}
	
	@Column(name = "ntte_date")
	@JsonProperty("ntte_date")
	public Timestamp getNtteDate() {
		return ntteDate;
	}
	public void setNtteDate(Timestamp ntteDate) {
		this.ntteDate = ntteDate;
	}
	
	public BigDecimal getNominal() {
		return nominal;
	}
	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}
	
	@Column(name = "queue_ticket")
	@JsonProperty("queue_ticket")
	public String getQueueTicket() {
		return queueTicket;
	}
	public void setQueueTicket(String queueTicket) {
		this.queueTicket = queueTicket;
	}
	
	@Column(name = "last_eleven_digits_efiling_code")
	@JsonProperty("last_eleven_digits_efiling_code")
	public String getLastElevenDigitsEfilingCode() {
		return lastElevenDigitsEfilingCode;
	}
	public void setLastElevenDigitsEfilingCode(String lastElevenDigitsEfilingCode) {
		this.lastElevenDigitsEfilingCode = lastElevenDigitsEfilingCode;
	}
	
	@Column(name = "efiling_code")
	@JsonProperty("efiling_code")
	public String getEfilingCode() {
		return efilingCode;
	}
	public void setEfilingCode(String efilingCode) {
		this.efilingCode = efilingCode;
	}
	
	@Column(name = "tax_type")
	@JsonProperty("tax_type")
	public String getTaxType() {
		return taxType;
	}
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	
	@CreationTimestamp
	@Column(name = "created_at", nullable = false)
	@JsonProperty("created_at")
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	
	@UpdateTimestamp
	@Column(name = "updated_at", updatable = true, nullable = false)
	@JsonProperty("updated_at")
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	
	@Column(name = "tax_year")
	@JsonProperty("tax_year")
	public String getTaxYear() {
		return taxYear;
	}
	public void setTaxYear(String taxYear) {
		this.taxYear = taxYear;
	}
	
	@Column(name = "tax_period_end")
	@JsonProperty("tax_period_end")
	public String getTaxPeriodEnd() {
		return taxPeriodEnd;
	}
	public void setTaxPeriodEnd(String taxPeriodEnd) {
		this.taxPeriodEnd = taxPeriodEnd;
	}
	
	@Column(name = "tax_period_start")
	@JsonProperty("tax_period_start")
	public String getTaxPeriodStart() {
		return taxPeriodStart;
	}
	public void setTaxPeriodStart(String taxPeriodStart) {
		this.taxPeriodStart = taxPeriodStart;
	}
}