package com.efiling.pajak.io.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
@Entity
@Table(name = "fileModel")
public class FileModel {
	private long id;
    private String uploadDir;
    
    @Column(name = "upload_dir")
    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
    
    @Id
    @Column(name="id", nullable=false, insertable=false, updatable=false)
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}