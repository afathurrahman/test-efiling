package com.efiling.pajak.io.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "registration_log")
public class RegistrationLogModel {
	
	private long id;
	private long companyId;
	private String regId;
	private String code;
	private String message;
	private String content;
	private String efin;
	private String emailEfin;
	private String username;
	private String password;
	private String phone;
	private String aspId;
	private String registrarName;
	private String jobPosition;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	
	@Id
	@Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "company_id")
	@JsonProperty("company_id")
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	@Column(name = "reg_id")
	@JsonProperty("reg_id")
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getEfin() {
		return efin;
	}
	public void setEfin(String efin) {
		this.efin = efin;
	}
	
	@Column(name = "email_efin")
	@JsonProperty("email_efin")
	public String getEmailEfin() {
		return emailEfin;
	}
	public void setEmailEfin(String emailEfin) {
		this.emailEfin = emailEfin;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name = "asp_id")
	@JsonProperty("asp_id")
	public String getAspId() {
		return aspId;
	}
	public void setAspId(String aspId) {
		this.aspId = aspId;
	}
	
	@Column(name = "registrar_name")
	@JsonProperty("registrar_name")
	public String getRegistrarName() {
		return registrarName;
	}
	public void setRegistrarName(String registrarName) {
		this.registrarName = registrarName;
	}
	
	@Column(name = "job_position")
	@JsonProperty("job_position")
	public String getJobPosition() {
		return jobPosition;
	}
	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}
	
	@CreationTimestamp
	@Column(name = "created_at", nullable = false)
	@JsonProperty("created_at")
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	
	@UpdateTimestamp
	@Column(name = "updated_at", updatable = true, nullable = false)
	@JsonProperty("updated_at")
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
}
