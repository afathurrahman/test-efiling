package com.efiling.pajak.io.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "other_attachment_file")
public class OtherAttachmentFileModel {
	
	private long id;
	private long efilingId;
//	private EfilingModel efilling;
	private int status;
	private String pdfFileName;
	private String pdfFileData;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	
	@Id
	@Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "efiling_id")
	@JsonProperty("efiling_id")
	public long getEfilingId() {
		return efilingId;
	}
	public void setEfilingId(long efilingId) {
		this.efilingId = efilingId;
	}
	
//	public EfilingModel getEfilling() {
//		return efilling;
//	}
//	public void setEfilling(EfilingModel efilling) {
//		this.efilling = efilling;
//	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name = "pdf_file_name")
	@JsonProperty("pdf_file_name")
	public String getPdfFileName() {
		return pdfFileName;
	}
	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}
	
	@Column(name = "pdf_file_data")
	@JsonProperty("pdf_file_data")
	public String getPdfFileData() {
		return pdfFileData;
	}
	public void setPdfFileData(String pdfFileData) {
		this.pdfFileData = pdfFileData;
	}
	
	@CreationTimestamp
	@Column(name = "created_at", nullable = false)
	@JsonProperty("created_at")
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	
	@UpdateTimestamp
	@Column(name = "updated_at", updatable = true, nullable = false)
	@JsonProperty("updated_at")
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
}
