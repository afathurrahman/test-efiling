package com.efiling.pajak.io.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.efiling.pajak.io.exception.ResourceNotFoundException;
import com.efiling.pajak.io.model.EfilingModel;
import com.efiling.pajak.io.model.OtherAttachmentFileModel;
import com.efiling.pajak.io.rest.BaseResponse;
import com.efiling.pajak.io.rest.Page;
import com.efiling.pajak.io.rest.ShowEfilingsByStatus;
import com.efiling.pajak.io.rest.ShowOneResponse;
import com.efiling.pajak.io.service.EfilingService;
import com.efiling.pajak.io.service.FileProcessService;
import com.efiling.pajak.io.service.OtherAttachmentFileService;

@RestController
@CrossOrigin
@RequestMapping("/v2")
public class EfilingController {
	
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	private String[] pphBadanCodes = {"LK", "RK", "DN", "BUT", "MGS", "LU", "DL"};
	
	@Autowired
	EfilingService efilingService;
	
	@Autowired
	OtherAttachmentFileService otherAttachmentFileService;
	
	@Autowired
	FileProcessService fileProcessService;
	
	/*
	 * CREATE
	 */
	@PostMapping("/company/{company_id}/efiling/upload")
	public ResponseEntity<BaseResponse<Boolean>> uploadMultipleFiles(@PathVariable("company_id") long companyId, @RequestParam("files") MultipartFile[] files) {
		
		/**
		 * The first file must be csv file.
		 * - firstFileNameAndExt[0] = File name
		 * - firstFileNameAndExt[1] = File extension.
		 */
		String[] firstFileNameAndExt = StringUtils.cleanPath(files[0].getOriginalFilename()).split("\\.");
		
		BaseResponse<Boolean> response = new BaseResponse<Boolean>();
		
		boolean isSptBadan = false;
		
		LinkedHashMap<String, String> fileMap = new LinkedHashMap<String, String>();
		
		// Cek file utama adalah csv
		if (!firstFileNameAndExt[1].equalsIgnoreCase("csv")) {
			response.setStatus(false);
			response.setMessage("Unggah SPT gagal. Pastikan file utama merupakan file berformat csv.");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
		}
		
		// Cek ukuran file csv tidak lebih dari 10 MB
		if (files[0].getSize() > 10000) {
			response.setStatus(false);
			response.setMessage("Unggah SPT gagal. Pastikan file utama (CSV) berukuran tidak lebih dari 10MB.");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
		}
		
		// Cek nama file csv sesuai dengan spek djp
		// Dummy NPWP Fintax
		String companyNpwp = "844507533003000";
		if (!firstFileNameAndExt[0].substring(0, 15).equalsIgnoreCase(companyNpwp)) {
			response.setStatus(false);
			response.setMessage("Unggah SPT gagal. NPWP pada file tidak sesuai dengan NPWP perusahaan.");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
		}
		
		/*
		 * Cek apakah csv spt badan atau badan dolar
		 * - Kode SPT PPh Badan: F113214
		 * - Kode SPT PPh Badan Dolar: F113215
		 */
		if (firstFileNameAndExt[0].substring(25, 32).equalsIgnoreCase("F113214")) {
			isSptBadan = true;
		}
		
		fileMap.put("csvFileName", firstFileNameAndExt[0]);
		fileMap.put("csvFileExt", firstFileNameAndExt[1]);
		
		if (files.length == 2) { // Untuk SPT apapun, wajib CSV dan PDF
			String[] secondFileNameAndExt = StringUtils.cleanPath(files[1].getOriginalFilename()).split("\\.");
			
			if (isSptBadan == false) { // Bukan SPT Badan, Hanya perlu 1 CSV dan 1 PDF
				if (!secondFileNameAndExt[0].equalsIgnoreCase(firstFileNameAndExt[0])) {
					response.setStatus(false);
					response.setMessage("Unggah SPT gagal. PDF yang ingin diunggah tidak sesuai dengan CSV yang ingin diunggah.");
					return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
				}
			}
			
			fileMap.put("firstPdfName", secondFileNameAndExt[0]);
			fileMap.put("firstPdfExt", secondFileNameAndExt[1]);
			
			
		} else if (files.length > 2 && isSptBadan == true) { // Untuk saat ini hanya berlaku untuk SPT PPh Badan
			String fileNameLK = firstFileNameAndExt[0] + "LK";
			for (int i = 2; i < files.length; i++) {
				String[] fileNameAndExt = StringUtils.cleanPath(files[i].getOriginalFilename()).split("\\.");
				
				if (!fileNameAndExt[0].substring(0, 36).equalsIgnoreCase(firstFileNameAndExt[0])) {
					response.setStatus(false);
					response.setMessage("Unggah SPT gagal. PDF yang ingin diunggah tidak sesuai dengan CSV yang ingin diunggah.");
					return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
				}
				
				// Urutan file: CSV, PDF, PDF LK, PDF lain yg lainnya
				
				if (i == 2) {
					if (!fileNameAndExt[0].equalsIgnoreCase(fileNameLK)) {
						response.setStatus(false);
						response.setMessage("SPT PPh Badan wajib melampirkan laporan keuangan (LK).");
						return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
					}
				}
				
				if (!Arrays.asList(pphBadanCodes).contains(fileNameAndExt[0].substring(36, fileNameAndExt[0].length()))) {
					response.setStatus(false);
					response.setMessage("Penamaan file untuk dokumen lainnya PPh Badan salah.");
					return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
				}
				
//				if (fileNameAndExt[0].substring(36, fileNameAndExt[0].length())) {
//					
//				}
				
				fileMap.put(i + "PdfName", fileNameAndExt[0]);
				fileMap.put(i + "PdfExt", fileNameAndExt[1]);
			}
		} else {
			;
		}
		
		/*
		 * Upload
		 */
		efilingService.storeFiles(files, companyId, isSptBadan, fileMap);
		
		response.setStatus(true);
		response.setMessage("File CSV dan PDF telah berhasil diunggah.");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/*
	 * UPDATE PDF ONLY
	 */
	@PutMapping("/company/{company_id}/efiling/{efiling_id}/update")
	public ResponseEntity<BaseResponse<Boolean>> updateMultipleFiles(
			@PathVariable("company_id") long companyId,
			@PathVariable("efiling_id") long efilingId,
			@RequestParam("files") MultipartFile[] files) throws ResourceNotFoundException {
		
		BaseResponse<Boolean> response = new BaseResponse<Boolean>();
		
		EfilingModel target = efilingService.findByIdAndCompanyId(efilingId, companyId)
				.orElseThrow(() -> new ResourceNotFoundException("e-Filing not found for this id: " + efilingId));
		
		if (target.getStatus() != 0) {
			response.setStatus(false);
			response.setMessage("Tidak bisa mengubah file.");
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
		}
		
		List<String> otherAttachmentFilesName;
		boolean isSptBadan = (target.getCsvFileName().substring(25, 32).equalsIgnoreCase("F113214")) ? true : false;
		
		if (isSptBadan == true) {
			otherAttachmentFilesName = otherAttachmentFileService.findAllNameByEfilingId(efilingId);
		}
		
		for (MultipartFile file: files) {
			
			if (!file.getOriginalFilename().split("//.")[0].substring(0, 36).equalsIgnoreCase(target.getPdfFileName())) {
				response.setStatus(false);
				response.setMessage("Unggah untuk mengubah gagal. Pastikan nama file PDF sesuai dengan nama file CSV");
				return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
			}
			
			/*
			 * Other document exists and isSptBadan = true
			 */
			if (StringUtils.cleanPath(file.getOriginalFilename()).split("\\.")[0].length() > 36) {
				otherAttachmentFilesName = otherAttachmentFileService.findAllNameByEfilingId(efilingId);
				
				if (!otherAttachmentFilesName.contains(file.getOriginalFilename().split("\\.")[0])) {
					response.setStatus(false);
					response.setMessage("Unggah untuk mengubah gagal. Pastikan nama file PDF sesuai dengan nama file PDF sebelumnya.");
					return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
				}
			}
			
			efilingService.storeFile(companyId, efilingId, file);
			
		}
		
		response.setStatus(true);
		response.setMessage("e-Filing berhasil diubah.");
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
//	public void uploadFile(@RequestParam("file") MultipartFile file, long companyId) {
//		efilingService.storeFile(file, companyId);
//		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//				.path("/file/downloadFile/")
//				.path(fileName)
//				.toUriString();
//		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
//	}
	
	@GetMapping("/company/{company_id}/efiling/{efiling_id}/download/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, 
			@PathVariable("company_id") long companyId,
			@PathVariable("efiling_id") long efilingId,
			HttpServletRequest request) {
		
		Resource resource = efilingService.loadFileAsResource(fileName, companyId, efilingId);
		
		String contentType = null;
		
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException exc) {
			logger.info("Ekstensi file tidak sesuai.");
		}
		
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
	
	@DeleteMapping("/company/{company_id}/efiling/{efiling_id}/delete")
	public ResponseEntity<BaseResponse<String>> deleteEfiling(@PathVariable("company_id") long companyId, @PathVariable("efiling_id") long efilingId) throws ResourceNotFoundException {
		EfilingModel target = efilingService.findByIdAndCompanyId(efilingId, companyId)
				.orElseThrow(() -> new ResourceNotFoundException("e-Filing not found for this id: " + efilingId));
		
		efilingService.deleteEfiling(target);
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
	
	@GetMapping("/company/{company_id}/efiling/{efiling_id}")
	public ResponseEntity<ShowOneResponse> showOneEfiling(@PathVariable("company_id") long companyId, @PathVariable("efiling_id") long efilingId) throws ResourceNotFoundException, IOException {
		
		EfilingModel target = efilingService.findByIdAndCompanyId(efilingId, companyId)
				.orElseThrow(() -> new ResourceNotFoundException("e-Filing not found for this id: " + efilingId));
		
		ShowOneResponse response = new ShowOneResponse();
		response.setTaxType(target.getTaxType());
		response.setTaxPeriod(target.getTaxPeriodStart() + " - " + target.getTaxPeriodEnd());
		response.setTaxYear(target.getTaxYear());
		response.setRevision(target.getRevision());
		response.setStatus(target.getStatus());
		response.setReportDate(target.getReportDate());
		
		Resource csvResource = efilingService.loadFileAsResource(target.getCsvFileName() + ".csv", companyId, efilingId);
		Resource pdfResource = efilingService.loadFileAsResource(target.getPdfFileName() + ".pdf", companyId, efilingId);
	
		LinkedHashMap<String, Object> files = new LinkedHashMap<String, Object>();
		List<Object> pdfs = new ArrayList<Object>();
		
		files.put("csv", ServletUriComponentsBuilder.fromUri(csvResource.getURI()).toUriString());
		pdfs.add(ServletUriComponentsBuilder.fromUri(pdfResource.getURI()).toUriString());
		
		if (otherAttachmentFileService.findAllByEfilingId(efilingId).size() > 0) { 
			for (OtherAttachmentFileModel other: otherAttachmentFileService.findAllByEfilingId(efilingId)) {
				Resource otherPdf = efilingService.loadFileAsResource(other.getPdfFileName() + ".pdf", companyId, efilingId);
				pdfs.add(ServletUriComponentsBuilder.fromUri(otherPdf.getURI()).toUriString());
			}
		}
		
		files.put("pdf(s)", pdfs);
		
		response.setFiles(files);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/*
	 * MONTH = 01, 02, 10, 12
	 */
	@GetMapping("/company/{company_id}/efiling/{year}/{month}/{page}")
	public ResponseEntity<ShowEfilingsByStatus> showEfilingsByStatus(
			@PathVariable("company_id") long companyId,
			@PathVariable("year") String taxYear,
			@PathVariable("month") int taxPeriod,
			@PathVariable("page") int pageNumber) {
		
		ShowEfilingsByStatus efilingListResponse = new ShowEfilingsByStatus();
		 
		List<EfilingModel> efilingList = efilingService.getFilingListPage(companyId, taxYear, taxPeriod, pageNumber);
		Page page = new Page();
		
		/*
		 * Pagination, 20 rows each
		 */
		
		efilingListResponse.setEfilingList(efilingList);
		efilingListResponse.setPage(page);
		
		page.setPerPage(20);
		page.setTotalRecord(efilingList.size());
		efilingListResponse.setPage(page);
		
		return ResponseEntity.status(HttpStatus.OK).body(efilingListResponse);
	}
	
	@PostMapping("company/{company_id}/efiling/{efiling_id}/send_to_djp")
	public ResponseEntity<BaseResponse<Boolean>> sendSptToDjp(@PathVariable("company_id") long companyId, @PathVariable("efiling_id") long efilingId) throws ResourceNotFoundException, InterruptedException, ExecutionException {
		BaseResponse<Boolean> response = new BaseResponse<Boolean>();
		
		EfilingModel sendEfiling = efilingService.findByIdAndCompanyId(efilingId, companyId)
				.orElseThrow(() -> new ResourceNotFoundException("e-Filing not found for this id: " + efilingId));
		
		if (sendEfiling.getStatus() != 0) {
			response.setStatus(false);
			response.setMessage("Efiling sudah pernah dikirimkan ke DJP");
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
		}
		
		/*
		 * Send to DJP means user decide to report the SPT file,
		 * so report date must be updated/filled.
		 */
		sendEfiling.setReportDate(new Timestamp(System.currentTimeMillis()));
		efilingService.saveEfiling(sendEfiling);
		
		/*
		 * Async
		 */
//		CompletableFuture<Boolean> pfRes = efilingService.sendToProcessingFile(sendEfiling);
		efilingService.sendToProcessingFile(sendEfiling);
		
//		if (processResponse.getStatusCode() == HttpStatus.OK) {
		response.setStatus(true);
		response.setMessage("Efiling telah dikirimkan ke DJP");
		return ResponseEntity.status(HttpStatus.OK).body(response);
//		} else {
//			response.setStatus(false);
//			response.setMessage("Kode gagal: keterangan gagal");
//			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
//		}
	}
	
	@GetMapping("/test")
	public ResponseEntity<BaseResponse<Boolean>> test(
//			@RequestHeader("Cookie") String cookie,
//            @RequestHeader("aspId") String aspId,
//            @RequestHeader("aspPassword") String aspPassword,
//            @RequestBody HashMap<String, String> params
			) {
		
//		System.out.println(cookie);
//		System.out.println(aspId);
//		System.out.println(aspPassword);
//		
//		System.out.println(params.toString());
		
//		fileProcessService.getFilePath("");
		
//		System.out.println(fileProcessService.base64Encoder("8445075330030000101201700F1132031009"));
//		System.out.println(fileProcessService.base64Decoder("ODQ0NTA3NTMzMDAzMDAwMDEwMTIwMTcwMEYxMTMyMDMxMDA5"));
		
		efilingService.getCookieFromGateway();
		
		BaseResponse<Boolean> response = new BaseResponse<Boolean>();
		response.setMessage("Halo");
		response.setStatus(true);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
